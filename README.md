Sensor Project
===============================

Create a virtual environment for this project. We'll do this using
[virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
to keep things simple,
but you may also find something like
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/)
to be useful:

```bash
# Create a virtualenv in which we can install the dependencies
virtualenv env
source env/bin/activate
```

Now we can install our dependencies:

```bash
pip install -r requirements.txt
```

```bash
Install Postgresql database
```

Sample Script
-----------------
Sample Script committed in insert-script.sql file

Now setup our database:

```bash
# Setup the database
python manage.py migrate


```

Now you should be ready to start the server:

```bash
python manage.py runserver
```

Now head on over to

POST API
---------------
API --(POST) http://127.0.0.1:8000/api/sensor
Sample Payload -{reading: 26.0, timestamp: 1511161234, sensorType: “Temperature”}

GET API
---------------
API --(GET All Data) http://127.0.0.1:8000/api/sensor

API --(GET sensorType filter) http://127.0.0.1:8000/api/sensor?sensorType=aaa

