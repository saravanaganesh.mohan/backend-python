# Generated by Django 3.0.5 on 2020-04-14 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('reading', models.FloatField()),
                ('timestamp', models.BigIntegerField()),
                ('sensorType', models.TextField()),
            ],
            options={
                'db_table': 'sensor',
            },
        ),
    ]
