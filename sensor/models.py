from django.db import models


class Sensor(models.Model):
    id=models.AutoField(primary_key=True)
    reading=models.FloatField(blank=False)
    timestamp=models.BigIntegerField(blank=False)
    sensorType=models.TextField(blank=False)
    
    class Meta:
        db_table = 'sensor'