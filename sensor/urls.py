from django.conf.urls import url 
from sensor import views 
 
urlpatterns = [ 
    url(r'^api/sensor$', views.sensor_detail)
]