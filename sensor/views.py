from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from sensor.models import Sensor 
from sensor.serializers import SensorSerializer
from rest_framework.decorators import api_view
from django.db.models import Q


@api_view(['POST','GET'])
def sensor_detail(request):
    if request.method == 'POST':
        sensor_data = JSONParser().parse(request)
        sensor_serializer = SensorSerializer(data=sensor_data)
        if sensor_serializer.is_valid():
            sensor_serializer.save()
            return JsonResponse(sensor_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(sensor_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        filters = None
        sensorType = request.GET.get('sensorType', None)
        startDate=request.GET.get('startDate', None)
        endDate=request.GET.get('endDate', None)
        if sensorType is not None:
            filters = Q(sensorType__icontains=sensorType)
        if startDate is not None and endDate is not None:
            filters = Q(timestamp__gt=startDate) & Q(timestamp__lt=endDate)
        if sensorType is not None and startDate is not None and endDate is not None : 
            filters = Q(sensorType__icontains=sensorType,timestamp__range=(startDate, endDate))
        if filters is not None:
            sensor = Sensor.objects.filter(filters).order_by('id')
        else:
            sensor = Sensor.objects.all().order_by('id')
        sensor_serializer = SensorSerializer(sensor, many=True)
        return JsonResponse(sensor_serializer.data, safe=False)